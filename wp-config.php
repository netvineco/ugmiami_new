<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'nv_ugmiami');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'root');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'root');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R5G$qCIyN$qoZl|@mD/hXFA5ru#u|Rg|a*c98}/ ,+c`)-{e;UcekTaZOd,eI94g');
define('SECURE_AUTH_KEY',  'Hjnm-0pq-GSLL7V-q/O+y;~ZZrN2*o0Opu@Q$J<.$L1skj9t|2V~(C%M [AsCOx ');
define('LOGGED_IN_KEY',    'cI28%T!F84SwrG`u+?a1L]M8RXt5?N^XL0|+>BB.[wKZcTT2WRdU>4peE^K+gQk0');
define('NONCE_KEY',        'ZnY^+NJ_VMJ!ef] CSdZO|1<Z1{Hz+#+o75t^+9A907M,^z5iIiphvd.Db4Pt]3|');
define('AUTH_SALT',        '+Y+pK:]oORO5|n3$UjQ7=W|<5wgcwjez4hCm_gwXH(nH/rzhZay::tUtoU~STe{!');
define('SECURE_AUTH_SALT', 'Sl$d*q,pHOic=PM,sD3^O2Mc9On%+,7d)m+KK&(<v^pDwPG7sF1P.Lrz~<w)fc1M');
define('LOGGED_IN_SALT',   '>=:fL!gs+b4?8z;2:f^J_hb/fC,.2GxX-)KEv^?MyQF2rG*%W4cz+6>~=&_3Rn:f');
define('NONCE_SALT',       '(3=~HIWRp,ONKD;XRwj((gtoNl<7cCEXJ*jD)]OKC.al[:j4N6@W7staV~Ts=,1J');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ugm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');


/**
 * Set custom paths
 *
 * These are required because wordpress is installed in a subdirectory.
 */
if (!defined('WP_SITEURL')) {
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp');
}
if (!defined('WP_HOME')) {
	define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] . '');
}
if (!defined('WP_CONTENT_DIR')) {
	define('WP_CONTENT_DIR', dirname(__FILE__) . '/app');
}
if (!defined('WP_CONTENT_URL')) {
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/app');
}


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
